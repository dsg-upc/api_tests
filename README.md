# API tests

This repository contains client side test scripts for the eReuse API.

- [index.js](index.js): Sends a burst of requests to the desired API endpoint and collects time and ratio metrics.
- [validation.js](validation.js): A sequence of operations that validate the correct operation of the API calls.
