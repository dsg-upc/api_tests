const web3 = require('web3');
const fs = require('fs')
const axios = require('axios')
const crypto = require(`crypto`)

const nTx = process.argv[2];
var endedTxs = 0
var results = new Array(nTx+1)

function writeJSON() {
    if (endedTxs == nTx) {
        const totalTime = fs.createWriteStream(`totalTime-${nTx}-Tx.txt`, {flags:'a'})
        const totalRate = fs.createWriteStream(`totalRate-${nTx}-Tx.txt`, {flags:'a'})
        const times = fs.createWriteStream(`times.txt`, {flags:'a'})
        const rates = fs.createWriteStream(`rates.txt`, {flags:'a'})
    
        let latencies = []
        let minLatency = results[0]['returnTime'] - results[0]['sentTime']
        let maxLatency = results[0]['returnTime'] - results[0]['sentTime']
        let max = results[0]['sentTime']
        let min = results[0]['returnTime']
        //ADD AVERAGE GAS PER TX
        for (let id = 0; id < nTx; id++) {
            sentTime = results[id]['sentTime']
            returnTime = results[id]['returnTime']
            if (sentTime < min) min = sentTime;
            if (returnTime > max) max = returnTime;
            //latencia
            let latency = returnTime - sentTime;
            latencies.push(latency)
            if (latency < minLatency) minLatency = latency;
            if (latency > maxLatency) maxLatency = latency;
        }

        results[nTx] = {}
        results[nTx]['x'] = nTx
        results[nTx]['totalTime'] = max - min
        results[nTx]['totalRate'] = nTx / ((max - min) / 1000)
        const json = JSON.stringify(results, null, "\t");
        console.log(json)
        fs.writeFile(`./results-${nTx}.json`, json, err => {
            if (err) {
                console.log('Error writing file', err)
            } else {
                console.log('Successfully wrote file')
            }
        })

        console.log(`Completed ${nTx} transactions in ${max - min}ms.`)
        console.log(`${nTx / ((max - min) / 1000)} tx/s`)
        latencySum = latencies.reduce(function (a, b) {
            return a + b;
        }, 0);
        avgLatency = latencySum / nTx

        totalTime.write(`${max - min}\n`)
        totalRate.write(`${nTx / ((max - min) / 1000)}\n`)
        let obj = {x:parseInt(nTx), y:max-min}
        times.write(`${JSON.stringify(obj)}\n`)
        obj = {x:parseInt(nTx), y:nTx / ((max - min) / 1000)}
        rates.write(`${JSON.stringify(obj)}\n`)

        console.log(`Average latency per transaction: ${avgLatency}ms`)
        console.log(`Minimum transaction latency: ${minLatency}ms`)
        console.log(`Maximum transaction latency: ${maxLatency}ms`)
    }
}

function sendTx(){
    for (let id = 0; id<nTx; id++){
        results[id] = {};
        results[id]['id'] = id;
        results[id]['sentTime'] = Date.now()
        axios
            .post('http://localhost:3005/api/devices/recycle', {
                // uid: crypto.randomBytes(20).toString('hex'),
                // initValue: "0",
                // owner: crypto.randomBytes(20).toString('hex')
                // proof_type: "ProofDataWipe",
                device_address: "0xA6cf2067A552E3eBBaA23002425d61936333813D",
                // erasureType: "TestType",
                // date: "12/07/2021",
                // erasureResult: "Success",
                // proofAuthor: "0x1608E7D037F313f940260DDf1b6F9aA5e7a5623E"
            })
            .then(res => {
                results[id]['returnTime'] = Date.now()
                results[id]["data"] = res.data
                endedTxs += 1;
                writeJSON();
            })
            .catch(error => {
                console.error(error)
            })
    }
}

sendTx();

