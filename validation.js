const ethers = require('ethers');
const fs = require('fs')
const axios = require('axios')
const crypto = require(`crypto`)
const _ = require('lodash');
const { SHA3 } = require('sha3');

const api_url = "http://localhost:3005"
const proofAuthor = "0x1608E7D037F313f940260DDf1b6F9aA5e7a5623E"
const destinyAddress = "0xd719f5806C9514b7EA1807Eb78Fe3594F56eC393"


async function test1() {
    console.log("Starting test...")
    try{
        console.log("\n1. Creating device...")
        var response = await axios.post(`${api_url}/api/devices/create`, {
            uid: crypto.randomBytes(20).toString('hex'),
            initValue: "0",
            owner: crypto.randomBytes(20).toString('hex')
        })
        //console.log(response)
        var device_address = response.data.data.deviceAddress
        if (!ethers.utils.isAddress(device_address)) throw "✘ Device Address is not valid on creation response."
        else console.log("✓ Device created successfully.")


        console.log("\n2. Generating a proof...")
        response = await axios.post(`${api_url}/api/devices/generateProof`, {
            proof_type: "ProofDataWipe",
            device_address: device_address,
            erasureType: "TestType",
            date: Date(),
            erasureResult: "true",
            proofAuthor: proofAuthor
        })
        var expectedData = {
            userAddress: proofAuthor,
            deviceAddress: device_address,
            erasureType: "TestType",
            erasureResult: true
        }
        if (!_.isEqual(response.data.data, expectedData)) throw "✘ Unexpected response on proof generation."
        else console.log("✓ Proof generated successfully.")


        console.log("\n3. Transferring device...")
        var new_registrant = crypto.randomBytes(20).toString('hex')
        response = await axios.post(`${api_url}/api/devices/transfer`, {
            deposit: "0",
            new_owner: destinyAddress,
            new_registrant: new_registrant,
            device_address: device_address
        })
        expectedData = {
            deviceAddress: device_address,
            new_owner: destinyAddress,
            new_registrant: new_registrant
        }
        if (!_.isEqual(response.data.data, expectedData)) throw "✘ Unexpected response on device transfer."
        else console.log("✓ Device transferred successfully.")

        
        console.log("\n4. Trying to transfer the same device (not owned anymore)...")
        var new_registrant = crypto.randomBytes(20).toString('hex')
        try{
            response = await axios.post(`${api_url}/api/devices/transfer`, {
                deposit: "0",
                new_owner: destinyAddress,
                new_registrant: new_registrant,
                device_address: device_address
            })
        } catch(err){
            console.log("✓ Device couldn't be transferred.")
        }
        expectedData = {
            deviceAddress: device_address,
            new_owner: destinyAddress,
            new_registrant: new_registrant
        }
        if (_.isEqual(response.data.data, expectedData)) throw "✘ Unexpected response on device transfer of a not owned device."


        console.log("\n5. Trying to recycle the same device (not owned anymore)...")
        try{
            response = await axios.post(`${api_url}/api/devices/recycle`, {
                device_address: device_address,
            })
        } catch(err){
            console.log("✓ Device couldn't be recycled.")
        }
        expectedData = {
            deviceAddress: device_address
        }
        if (_.isEqual(response.data.data, expectedData)) throw "✘ Unexpected response on device recycle of a not owned device."

        
        console.log("\n6. Recycling an owned device...")
        console.log("- Creating a new device...")
        response = await axios.post(`${api_url}/api/devices/create`, {
            uid: crypto.randomBytes(20).toString('hex'),
            initValue: "0",
            owner: crypto.randomBytes(20).toString('hex')
        })
        device_address = response.data.data.deviceAddress
        if (!ethers.utils.isAddress(device_address)) throw "✘ Device Address is not valid on creation response."
        else console.log("✓ Device created successfully.")
        console.log("- Recycling device...")
        response = await axios.post(`${api_url}/api/devices/recycle`, {
            device_address: device_address,
        })
        expectedData = {
            deviceAddress: device_address
        }
        if (!_.isEqual(response.data.data, expectedData)) throw "✘ Unexpected response on device recycle."
        else console.log("✓ Device recycled successfully.")


        console.log("\n7. Creating stamp on a random SHA3 hash...")
        var hash = new SHA3(256);
        hash.update(crypto.randomBytes(20).toString('hex'));
        hash = hash.digest('hex');
        //console.log(hash)
        response = await axios.post(`${api_url}/api/devices/createStamp`, {
            hash: hash
        })
        // console.log(response)
        if(response.data.data.hash == hash && response.data.data.timestamp > 0) console.log("✓ Stamp created successfully.")
        else throw "✘ Unexpected response on stamp creation."


        console.log("\n8. Checking existence of the recently created stamp...")
        response = await axios.post(`${api_url}/api/devices/checkStamp`, {
            hash: hash
        })
        // console.log(response.data.data.stamps)
        if(response.data.data.stamps[0].hash == hash && response.data.data.stamps[0].date > 0) console.log("✓ Stamp exists.")
        else throw "✘ Unexpected response on stamp check."


        console.log("\n9. Trying to create stamp on the same hash...")
        try{
            response = await axios.post(`${api_url}/api/devices/createStamp`, {
                hash: hash
            })
            if(response.data.data.hash == hash && response.data.data.timestamp > 0) console.log("✘ Unexpected response on stamp creation.")
        } catch(err){
            console.log("✓ Stamp couldn't be created.")
        }


        console.log("\n10. Checking an unexisting stamp of a random SHA3 hash...")
        hash = new SHA3(256);
        hash.update(crypto.randomBytes(20).toString('hex'));
        hash = hash.digest('hex');
        try{
            response = await axios.post(`${api_url}/api/devices/checkStamp`, {
                hash: hash
            })
            if(response.data.data.stamps[0].hash == hash && response.data.data.stamps[0].date > 0) console.log("✘ Unexpected response on stamp check.")
        } catch(err){
            console.log("✓ Stamp doesn't exist.")
        }


        console.log("\nAll tests passed!")




    } catch(err){
        console.log("\n Test failed!")
        console.log("Reason: "+err)
    }
}


test1()

